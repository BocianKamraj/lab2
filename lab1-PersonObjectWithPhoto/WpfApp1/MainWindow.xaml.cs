﻿
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Http;
using System.Net;
using System.IO;

namespace WpfApp1
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BitmapImage Photo;
        
        async Task<int> AccessTheWebAsync(int number)
        {
            using (HttpClient client = new HttpClient())
            {
                Task<string> getStringTask = client.GetStringAsync("https://pl.wikipedia.org/wiki/Wrocław");

                if (number < 0)

                    throw new ArgumentOutOfRangeException("number", number, "The number must be greater or qeual zero");
                int result = 0;
                while (result < number)
                {
                    result++;
                    await Task.Delay(100);
                }
             
                string urlContents = await getStringTask;

                return urlContents.Length;
            }
        }
        protected void UpdateProgressBlock(string text, TextBlock textBlock)
        {
            try
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    textBlock.Text = text;
                });
            }
            catch { }
        }
        class WaitingAnimation
        {
            private int maxNumberOfDots;
            private int currentDots;
            private MainWindow sender;


            public WaitingAnimation(int maxNumberOfDots, MainWindow sender)
            {
                this.maxNumberOfDots = maxNumberOfDots;
                this.sender = sender;
                currentDots = 0;
            }
            public void CheckStatus(Object stateInfo)
            {
                sender.UpdateProgressBlock(
                    "Oczekiwanie na pobranie zawartości strony." +
                    new Func<string>(() => {
                        StringBuilder strBuilder =new StringBuilder(string.Empty);
                for (int i = 0; i < currentDots; i++)
                    strBuilder.Append(".");
                return strBuilder.ToString();
            })(), sender.progressTextBlock
                );

                if(currentDots == maxNumberOfDots)
                currentDots=0;
                else
                currentDots++;
            }
           
    }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
     
        ObservableCollection<Person> people = new ObservableCollection<Person>
        {
           new Person { Name="P1", Age=1},
           new Person {Name="P2", Age=2}
        };
        public ObservableCollection<Person> Items
        {
            get => people;
        }
     

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

    
        private void AddNewPersonButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                people.Add(new Person { Age = int.Parse(ageTextBox.Text), Name = nameTextBox.Text, ProfilePhoto = Photo});
            }
            catch (System.FormatException)
            {
                MessageBox.Show("Wpisz prawidłowe dane");
            }
        }


         private void Button_Click(RoutedEventArgs e, object sender)
        {
                  OpenFileDialog op = new OpenFileDialog();
                  op.Title = "Select a picture";
                  op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                    "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                    "Portable Network Graphic (*.png)|*.png";
                  if (op.ShowDialog() == true)
                  {
                      Obraz.Source = new BitmapImage(new Uri(op.FileName));
                      Photo = new BitmapImage(new Uri(op.FileName));
                  }
              }
          
       
        private async void Przycisk_Click(object sender, RoutedEventArgs e)
    {
              try
              {
                  int finalNumber = int.Parse(this.finalNumberTextBox.Text);
                  var getResultTask = AccessTheWebAsync(finalNumber);
                  var waitingAnimationTask =
                      new System.Threading.Timer(
                          new WaitingAnimation(10, this).CheckStatus,
                          null,
                          TimeSpan.FromMilliseconds(0),
                          TimeSpan.FromMilliseconds(500)
                          );
                  var waitingAnimationTask2 = new System.Timers.Timer(100);
                  waitingAnimationTask2.Elapsed +=
                      (innerSender, innerE) =>
                      {
                          this.UpdateProgressBlock(
                              innerE.SignalTime.ToLongTimeString(),
                              this.progressTextBlock2);
                      };
                  waitingAnimationTask2.Disposed +=
                      (innerSender, innerE) =>
                      {
                          this.progressTextBlock2.Text = "Zadanie wykonane" + " Nazwa strony to: https://pl.wikipedia.org/wiki/Wrocław.";
                      };
                  waitingAnimationTask2.Start();
                  int result = await getResultTask;
                  waitingAnimationTask.Dispose();
                  waitingAnimationTask2.Dispose();
                  this.progressTextBlock.Text = "Znaleziony rezultat: " + result + " To długość strony wczytanej strony.";
              }
              catch(Exception ex)
              {
                  this.progressTextBlock.Text = "Error!" + ex.Message;

              }
              
           
        }
      



        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void FinalNumberTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}



